
# Ralph Hightower — github.io 

I freeze time; I photograph. I have 50++ years of computer programming and
software development; most recently using C# and .Net. Athough I am retired 
from software development as a vocation, I am still active for personal projects.
I have witnessed two final space flights: the US half of the Apollo/Soyuz Test 
Project, and Space Shuttle STS-135 Atlantis with the Final Four \[CDR Chris 
Ferguson, PLT Doug Hurley, MS1 Sandra Magnus, MS2 Rex Walheim\], as well as 
the final landing next to the control tower of the Shuttle Landing Facility 
(200 yards from the runway).

Photography is another creative endeavor that I have enjoyed since 1980. My
first camera was the Canon A-1 which was an innovative camera for the 80's,
which I still use. I added one of my bucket list cameras in July 2013, a 
Canon F-1N with the AE Finder FN and the AE Motor Drive FN. December 2013,
I added a DSLR, the Canon 5D Mark III with the EF 24-105mm f4L. I enjoy 
photography period. It doesn't matter if I am shooting film or digital. 

| Content | Website |
|---------|---------|
| CodeProject | [RalphHightower](https://www.codeproject.com/Members/RalphHightower)
| GitHub | [RalphHightower](https://ralphhightower.github.io/RalphHightower/) |
| GitLab | [RalphHightower](https://gitlab.com/RalphHightower) |
| Google Dev | [RalphHightower](https://g.dev/RalphHightower) |
| Gravatar | [RalphHightower](http://gravatar.com/ralphhightower) |
| Hackster.io | [ralph-hightower](https://www.hackster.io/ralph-hightower)
| LinkedIn Profile | [RalphHightower](https://www.linkedin.com/in/ralphhightower/)|
| Photography Portfolio \(Flickr\) | [RalphHightower](https://www.flickr.com/photos/ralphhightower/) |
| Stack Overflow | [ralph-hightower](https://stackoverflow.com/users/19978043/ralph-hightower) |
| YouTube | [RalphHightower](https://youtube.com/user/RalphHightower) |

| Links |
|-------|
| [Ralph Hightower Top Level Repository](https://ralphhightower.github.io/RalphHightower/) |
| [Camera Manuals, Firmware, Software](https://ralphhightower.github.io/RalphHightower/CanonFirmwareSoftware.html) |
| [Azure Resources](https://ralphhightower.github.io/Azure-Resources/) |
| [GM Canada Solution to Cellular CDMA 2G Sunset for OnStar](GM-Canada-Onstar-G2-Sunset) |
| [SpaceShuttleMissionSchedule: NASA Space Shuttle Mission Schedule Transfer to Outlook Calendar](https://ralphhightower.github.io/SpaceShuttleMissionSchedule/)[^1] |
| [Governments on GitHub](https://government.github.com/community/) |

[^1]: Support of SpaceShuttleMissionSchedule ended with wheelstop of Space Shuttle Atlantis on July 21, 2011 with the end of the Space Shuttle fleet. 
